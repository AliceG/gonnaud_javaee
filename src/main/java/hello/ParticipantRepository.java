package hello;

import org.springframework.data.repository.CrudRepository;


public interface ParticipantRepository extends CrudRepository<Participant, Integer> {
    
}