/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hello;

//import org.hibernate.tutorial.annotations.Event;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;



/**
 *
 * @author formation
 */
@Entity
@Table( name = "participant" )
public class Participant {
    
    public Participant() {
        this.nom = "def";
        this.prenom = "def";
        this.email = "def";
        this.datenaiss = LocalDate.parse("2000-01-01");
        this.evenements = new ArrayList<>();
    }
    
    public Participant(String nom, String prenom, String email, LocalDate datenaiss) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.datenaiss = datenaiss;
        this.evenements = new ArrayList<>();
    }
    
    @ManyToMany
    private List<Evenement> evenements;
    
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name="num_pers")
    private int numpers;
    
    @Column(name="nom", nullable=false)
    private String nom;
    
    @Column(name="prenom", nullable=false)
    private String prenom;
    
    @Column(name="email", nullable=false)
    private String email;

    @Column(name="date_naiss", nullable=false)
    private LocalDate datenaiss;
    
    @Column(name="organisation")
    private String organisation;
    
    @Column(name="observations")
    private String observations;
    
    public int getID() {
        return this.numpers;
    }
    
    public void setNom(String nom_nouv) {
        this.nom = nom_nouv;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public void setPrenom(String prenom_nouv) {
        this.prenom = prenom_nouv;
    }
    
    public String getPrenom() {
        return this.prenom;
    }
    
    public String getNomPrenom() {
        return this.prenom + " " + this.nom;
    }
    
    public void setEmail(String email_nouv) {
        this.email = email_nouv;
    }
    
    public void setDate(LocalDate date_nouv) {
        this.datenaiss = date_nouv;
    }
    
    public void addEvenementToList(Evenement evenement) {
        this.evenements.add(evenement);
    }
    
    public List<Evenement> getEvenements() {
        return this.evenements;
    }
    
    public void setEvenements(List<Evenement> evenements) {
        this.evenements = evenements;
    }
    
}
