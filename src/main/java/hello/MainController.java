package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;

import hello.EvenementRepository;
import hello.ParticipantRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.ui.Model;



@Controller    // This means that this class is a Controller
@RequestMapping(path="/appli") // This means URL's start with /demo (after Application path)
public class MainController {
    
	@Autowired 
        private ParticipantRepository participantRepository;
        @Autowired
        private EvenementRepository evenementRepository;

        /* METHODES GET pour requetes directes avec parametres */
	
        // Ajouter un participant
        // Ex : http://localhost:8080/appli/addp?nom=a&prenom=b
        @GetMapping(path="/addp") // Map ONLY GET Requests
	public @ResponseBody String addNewParticipant (@RequestParam String nom, @RequestParam String prenom) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request

		Participant p = new Participant();
                p.setNom(nom);
                p.setPrenom(prenom);
		participantRepository.save(p);
		return "Participant enregistre";
	}
        
        // Ajouter un evenement 
        // Ex : http://localhost:8080/appli/adde?intitule=c
        @GetMapping(path="/adde") 
	public @ResponseBody String addNewEvenement (@RequestParam String intitule) {
		Evenement ev = new Evenement();
                ev.setIntitule(intitule);
                evenementRepository.save(ev);
		return "Evenement " + ev.getIntitule() + " enregistre" ;
	}
        
        // Ajouter un participant a un evenement
        // Ex : http://localhost:8080/appli/addptoe?numpers=6&numeven=3
        @GetMapping(path="/addptoe")
	public @ResponseBody String addParticipantEvenement (@RequestParam int numpers, @RequestParam int numeven) {
                Optional<Evenement> ev = this.evenementRepository.findById(numeven);
                if (ev.isPresent()) {
                    Optional<Participant> p = this.participantRepository.findById(numpers);
                    if (p.isPresent()) {
                        ev.get().addParticipantToEvenement(p.get());
                        this.evenementRepository.save(ev.get());
                        this.participantRepository.save(p.get());
                    } else {
                        return "Participant " + Integer.toString(numpers) + " not found";
                    }
                } else {
                    return "Evenement " + Integer.toString(numeven) + " not found";
                }
                
		return "Participant enregistre a l'evenement" ;
	}

        
        
        /* METHODES liees aux vues */

        // Afficher tous les participants
	@GetMapping(path="/allp")
	public String allp(Model model) {
            Iterable<Participant> p_list = participantRepository.findAll();
            model.addAttribute("participants", p_list);
            return "allp";
	}
        
        // Afficher tous les evenements
        @GetMapping(path="/alle")
	public String alle(Model model) {
            Iterable<Evenement> e_list = evenementRepository.findAll();
            model.addAttribute("evenements", e_list);
            return "alle";
	}
        
        // Formulaire pour ajouter un participant
        @GetMapping("/formp")
        public String formp(Model model) {
            model.addAttribute("participant", new Participant());
            model.addAttribute("allEvenements", evenementRepository.findAll());
            return "formp";
        }
        
        // Traitement du formulaire pour ajouter un participant
        @PostMapping("/formpsave")
        public String formpsave(@ModelAttribute Participant participant, Model model) {
            participantRepository.save(participant);
            return "redirect:/appli/allp";
        }
        
        // Formulaire pour ajouter un evenement
        @GetMapping("/forme")
        public String forme(Model model) {
            model.addAttribute("evenement", new Evenement());
            return "forme";
        }
        
        // Traitement du formulaire pour ajouter un evenement
        @PostMapping("/formesave")
        public String formesave(@ModelAttribute Evenement evenement, Model model) {
            evenementRepository.save(evenement);
            return "redirect:/appli/alle";
        }

}