/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hello;

import hello.Participant;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author formation
 */
@Entity
@Table( name = "evenement" )
public class Evenement {

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "evenements")
    private List<Participant> participants;
    
    public Evenement() {
        this.intitule = "def";
        this.participants = new ArrayList<>();
    }
    
    public Evenement(String intitule) {
        this.intitule = intitule;
        this.participants = new ArrayList<>();
    }
    
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name="num_even")
    private int numeven;
    
    @Column(name="intitule", nullable=false)
    private String intitule;
    
    public int getID() {
        return this.numeven;
    }
    
    public void setIntitule(String intitule_nouv) {
        this.intitule = intitule_nouv;
    }
    
    public String getIntitule() {
        return this.intitule;
    }
    public void addParticipantToEvenement(Participant participant) {
        this.participants.add(participant);
        participant.addEvenementToList(this);
    }
    
}
