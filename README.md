# Résumé
Projet Java EE : développement d'une mini application web de gestion de participants à des événements

Technologies et notions mises en oeuvre : Modèle MVC ; Maven, Spring Boot, Postgres, Hibernate, Thymeleaf 

# Auteur
Alice Gonnaud

Mars 2019

# Dépôt du code

Le code est déposé sur GitLab à l'adresse suivante :

https://gitlab.com/AliceG/gonnaud_javaee/

Le dépôt est public.

# Executer le code 

* Clôner le dépôt
* Assurer la configuration de la base de données à utiliser pour le projet (fichier _src/main/resources/application.properties_)
* Compiler et exécuter le code (projet Java avec Maven)
* Ouvrir le navigateur. La page d'accueil est disponible à l'adresse `http://localhost:8080/`, qui permet de naviguer vers les différentes vues et d'intéragir avec le modèle.

# Description du dépôt

Le code source est à la racine. 

Il s'agit d'un projet Maven, qui en suit donc les conventions d'organisation.

Dans ce répertoire :

* Le _pom.xml_

* Répertoire _src/main/java/hello_ : codes sources (classes java)
    * Point d'entrée de l'application Spring : _Application.java_
    * Modèle : _Evenement.java_ et _Participant.java_
    * Contrôleur : _MainController.java_ (commenté dans le code pour plus de détails)
    * Répertoires : _EvenementRepository.java_ et _ParticipantRepository.java_

* Répertoire _src/main/resources_ : vues (HTML)
    * Répertoire _static_ : fichier _index.html_ : Point d'entrée, accessible à l'adresse `http://localhost:8080/`.
    Redirige vers les vues _templates_.
    * Répertoire _templates_ : Vues liées au modèle, gérées par le contrôleur (HTML avec thymeleaf). Voir la classe _MainController_ pour plus de détails.
        * _alle_ : Liste des événements
        * _allp_ : Liste des participants
        * _forme_ : Formulaire d'ajout d'un événement
        * _formp_ : Formulaire d'ajout d'un participant
    * _application.properties_ : Configuration de la BDD pour la persistance des données


# Fonctionnalités développées 

Toutes les fonctionnalités demandées a minima sont présentes ; avec quelques ajouts. Citons ainsi finalement : 

* Contrôleur dédié par vue 
* Affichage de la liste des participants (tableau Nom / Prénom)
* Affichage de la liste des événements (tableau Intitulé)
* Formulaire pour ajouter un événement : avec son intitulé
* Formulaire pour ajouter un participant : avec son nom, son prénom, et un ou plusieurs événements auxquels il participe
* Persistance des données : table _evenement_, table _participant_, table d'association _participant_evenements_
* Notons que les événements et les participants sont liés dans la BDD, bien que les vues ne le mettent pas en évidence (nous invitons donc le lecteur à consulter la BDD directement). On a une table d'association, avec une relation n-n (ManyToMany), puisqu'un même participant peut participer à plusieurs événements, et qu'un événement peut avoir plusieurs participants.

Notons que le projet est ouvert aux ajouts : les vues sont minimales (pas de stylisation), tous les champs des tables déclarés (e.g. Participant) ne sont pas utilisés pour simplifier les manipulations, seul l'ajout d'entités est implémenté... L'architecture fait cependant preuve de concept.

Par ailleurs des méthodes GET avec paramètres sont aussi disponibles pour agir sur la BDD sans passer par les vues : voir la classe _MainController_ (commentée) pour plus de détails.
